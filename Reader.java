import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Reader {

    final static String filename = "Activities.txt";
    List<MonitoredData> monitoredData = new ArrayList<>();

    public Reader(){ }

    public void read(){

        try {
            Stream<String> stream = Files.lines(Paths.get(filename));

            stream.forEach( line -> {
                String[] tokens =line.split("\t|\n");
                //System.out.println(tokens[0]+ " "+ tokens[2]+ " "+ tokens[4]);
                monitoredData.add(new MonitoredData(tokens[0],tokens[2],tokens[4]));
            });
            countDays(monitoredData);
            activityCount(monitoredData);
            duration(monitoredData);
            activityCountPerDay(monitoredData);
            filter(monitoredData);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void countDays(List<MonitoredData> monitoredDataList){

        int listLenght = monitoredDataList.size() - 1;
        String firstStartTime = monitoredDataList.get(0).start_time;
        String lastEndTime = monitoredDataList.get(listLenght).end_time;

        SimpleDateFormat format = new SimpleDateFormat("yyyy-ww-dd HH:mm:ss");
        long days = 0;
        try {
            Date date1 = format.parse(firstStartTime);
            Date date2 = format.parse(lastEndTime);
            days = getDifferenceDays(date2,date1);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("Number of days in <Activities.txt>: "+days);
    }
    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }
    public Map<String,Integer> activityCount(List<MonitoredData> monitoredData){
        Map<String,Integer> retMap = new HashMap<>();
        monitoredData.forEach(data -> {
                retMap.put(data.activity,0);
            });
        monitoredData.forEach(data -> {
            Integer count =retMap.get(data.activity);
            retMap.put(data.activity,count+1);
        });
        for (Map.Entry<String,Integer> entry: retMap.entrySet()) {
            System.out.println(entry.getKey()+" appears "+ entry.getValue()+" times");
        }
        return retMap;
    }
    ////////activity per day

    public void duration(List<MonitoredData> dataList){

        SimpleDateFormat format = new SimpleDateFormat("yyyy-ww-dd HH:mm:ss");

        dataList.forEach(line->{

            try {
                Date date1 = format.parse(line.start_time);
                Date date2 = format.parse(line.end_time);
                long durMin=0;
                long durSec=0;
                durMin = getDateDiff(date1,date2,TimeUnit.MINUTES);
                durSec = getDateDiff(date1,date2,TimeUnit.SECONDS);
                durSec = durSec - durMin*60;
                System.out.println(durMin+":"+durSec);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        });

    }
    public Map<Date,Map<String,Integer>> activityCountPerDay(List<MonitoredData> monitoredData){
        Map<Date,Map<String,Integer>> retMap = new HashMap<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        monitoredData.forEach(line ->{

            Date date = null;

            try {
                date = format.parse(line.start_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            if(date != null) {
                if (!retMap.containsKey(date)) {
                    Map<String, Integer> newMap = new HashMap<>();
                    newMap.put(line.activity, 1);
                    retMap.put(date, newMap);
                } else {
                    if (!retMap.get(date).containsKey(line.activity)) {
                        retMap.get(date).put(line.activity, 1);
                    }else {
                        retMap.get(date).put(line.activity,retMap.get(date).get(line.activity)+1);
                    }
                }
            }
        });

        retMap.forEach((k,v) -> {
            System.out.println(k);
            v.forEach((act,count) -> System.out.println(act +" "+count));
        });


//        monitoredData.forEach(data -> {
//
//            Map<String,Integer> insideMap = new HashMap<>();
//            insideMap.put(data.activity,0);
//
//            try {
//                Date date =format.parse(data.start_time);
//                retMap.put(date,insideMap);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//        });
//        monitoredData.forEach(data -> {
//            Date date = null;
//            try {
//                date = format.parse(data.start_time);
//
//                //System.out.println(retMap.get(date).get(data.activity)+1);
//
//                //retMap.get(data).put(data.activity,count);
//
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//
//        });
//        retMap.forEach((k,v) -> {
//            System.out.println(k);
//            v.forEach((k2,v2) -> System.out.println(k2+" "+v2) );
//        });
        return retMap;
    }

    public void filter(List<MonitoredData> monitoredData){

        Map<String,Long> retMap = new HashMap<>();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-ww-dd HH:mm:ss");

        monitoredData.forEach( line ->{

            Date dt1 = null;
            Date dt2 = null;
            long durMin = 0;

            try {
                dt1 = format.parse(line.start_time);
                dt2 = format.parse(line.end_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if (dt1 != null && dt2 != null) {
                durMin = getDateDiff(dt1, dt2, TimeUnit.MINUTES);
            }

           if(!retMap.containsKey(line.activity)){
               if(durMin<5){
                   retMap.put(line.activity, (long) 10101);
               }else {
                   retMap.put(line.activity, (long) 10100);
               }
           }else {
               if(durMin<5){
                   long  nr = retMap.get(line.activity);
                   nr += 101;
                   retMap.put(line.activity, nr);
               }else {
                   long  nr = retMap.get(line.activity);
                   nr += 100;
                   retMap.put(line.activity, nr);
               }
           }
        });
        retMap.forEach((k,v) -> {
            if(v%100 < 0.9*((v/100)%100)){
                System.out.println(k);
            }
        });



    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
    }

    public static void main(String[] args) {
        Reader red = new Reader();

        red.read();
    }

}
