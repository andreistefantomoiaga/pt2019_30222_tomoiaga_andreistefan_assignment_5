public class MonitoredData {

    String start_time;
    String end_time;
    String activity;

    public MonitoredData(String start_time,String end_time, String activity){
        this.start_time = start_time;
        this.end_time = end_time;
        this.activity = activity;
    }

}
